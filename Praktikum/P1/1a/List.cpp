#include "List.h"

List::List() {
	// Konstruktor f�r eine leere Liste
	head_tail = new Node;
	list_size = 0;
	temp = false;
	head_tail->next = head_tail;
	head_tail->prev = head_tail;
	head_tail->key = 0;
}


List::List(const List & _List) {
	// Konstruktor mit �bergabe einer Liste, die dann kopiert wird.
	// in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
	list_form = _List.list_form;
	head_tail = new Node;
	list_size = 0;
	temp = _List.temp;
	head_tail->next = head_tail;
	head_tail->prev = head_tail;
	head_tail->key = 0;

	Node * tmp_node;
	tmp_node = _List.head_tail->next;
	while (tmp_node != _List.head_tail) {
		head_tail->prev = new Node(tmp_node->key, head_tail->prev->next, head_tail->prev);
		head_tail->prev->prev->next = head_tail->prev;
		list_size++;
		tmp_node = tmp_node->next;
	}
	if (_List.temp) delete & _List;		// ist die �bergebene Liste eine tempor�re Liste? -> aus Operator +
}


List::List(const List * _List) {
	// Konstruktor mit �bergabe einer Liste, die dann kopiert wird.
	// in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
	list_form = _List->list_form;
	head_tail = new Node;
	list_size = 0;
	temp = _List->temp;
	head_tail->next = head_tail;
	head_tail->prev = head_tail;
	head_tail->key = 0;

	// copy all _List elements to this list
	Node * tmp_node;
	tmp_node = _List->head_tail->next;
	while (tmp_node != _List->head_tail) {
		head_tail->prev = new Node(tmp_node->key, head_tail->prev->next, head_tail->prev);
		head_tail->prev->prev->next = head_tail->prev;
		list_size++;
		tmp_node = tmp_node->next;
	}
	if (_List->temp) delete _List;		// ist die �bergebene Liste eine tempor�re Liste? -> aus Operator +
}


List::~List() {
	// Dekonstruktor
	// Alle Knoten der Liste m�ssen gel�scht werden, wenn die Liste gel�scht wird

	// only delete head tail if empty list
	if (this->list_size == 0) {
	    delete this->head_tail;
	    return;
	}

	// Start at head tail
	Node * node = this->head_tail;


	// Detach last from head tail
	this->head_tail->prev->next = nullptr;

	while (node->next != nullptr) {
	    node = node->next;
	    delete node->prev;
	}

    // loop ends at the last elem
    delete node;
    list_size = 0;
}


void List::insertFront(int key) {
	// Einf�gen eines neuen Knotens am Anfang der Liste

	// link new 1 -> 2
	Node * new_first = new Node(key, head_tail->next, head_tail);

	// relink 2 -> 1
	head_tail->next->prev = new_first;

	// link anchor -> 1
	head_tail->next = new_first;

	// increse size
	list_size++;
}


/**
 * Insert this list in front of another list
 * @param _List the other list
 */
void List::insertFront(List & _List) {
    // Einf�gen einer vorhandenen Liste am Ende
    /*
    Die einzuf�genden Knoten werden �bernommen (nicht kopiert)
    Die einzuf�gende Liste _List ist anschlie�end leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach this's last to List' first
    this->head_tail->prev->next = _List.head_tail->next;
    _List.head_tail->next->prev = this->head_tail->prev;


    // Attach _List's last to this' head_tail
    this->head_tail->prev = _List.head_tail->prev;
    _List.head_tail->prev->next = this->head_tail;


    // Detach _List's head_tail
    _List.head_tail->next = _List.head_tail;
    _List.head_tail->prev = _List.head_tail;

    this->list_size += _List.list_size;
    _List.list_size = 0;

/*
	Es wird ein Objekt �bergeben in dem Knoten vorhanden sein k�nnen.
	Diese Knoten (koplette Kette) werden an das Ende der Liste (this) angehangen ohne sie zu kopieren!
*/
}


/**
 * Insert this list in front of another list
 * @param _List the other list
 */
void List::insertFront(List * _List) {
    // Einf�gen einer vorhandenen Liste am Ende
    /*
    Die einzuf�genden Knoten werden �bernommen (nicht kopiert)
    Die einzuf�gende Liste _List ist anschlie�end leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach this's last to List' first
    this->head_tail->prev->next = _List->head_tail->next;
    _List->head_tail->next->prev = this->head_tail->prev;


    // Attach _List's last to this' head_tail
    this->head_tail->prev = _List->head_tail->prev;
    _List->head_tail->prev->next = this->head_tail;


    // Detach _List's head_tail
    _List->head_tail->next = _List->head_tail;
    _List->head_tail->prev = _List->head_tail;

    this->list_size += _List->list_size;
    _List->list_size = 0;

/*
	Es wird ein Objekt �bergeben in dem Knoten vorhanden sein k�nnen.
	Diese Knoten (koplette Kette) werden an das Ende der Liste (this) angehangen ohne sie zu kopieren!
*/
}


void List::insertBack(int key) {
	// Einf�gen eines neuen Knotens am Ende der Liste

    // link new last -> old last, new last -> head_tail
    Node * new_last = new Node(key, head_tail, head_tail->prev);

    // relink old last -> new last
    new_last->prev->next = new_last;

    // link anchor -> new last
    head_tail->prev = new_last;

    // increse size
    list_size++;
}


/**
 * Insert this list at the back of another list
 * @param _List the other list
 */
void List::insertBack(List & _List) {
    // Einf�gen einer vorhandenen Liste am Anfang
    /*
    Die einzuf�genden Knoten werden �bernommen (nicht kopiert)
    Die einzuf�gende Liste _List ist anschlie�end leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach _List's last to this' first
    this->head_tail->next->prev = _List.head_tail->prev;
    _List.head_tail->prev->next = this->head_tail->next;


    // Attach _List's first to this' head_tail
    this->head_tail->next = _List.head_tail->next;
    _List.head_tail->next->prev = this->head_tail;


    // Detach _List's head_tail
    _List.head_tail->next = _List.head_tail;
    _List.head_tail->prev = _List.head_tail;

    this->list_size += _List.list_size;
    _List.list_size = 0;
/*
	Es wird ein Objekt �bergeben in dem Knoten vorhanden sein k�nnen.
	Diese Knoten (koplette Kette) werden an den Anfang der Liste (this) �bertragen ohne sie zu kopieren!
*/
}


/**
 * Insert this list at the back of another list
 * @param _List the other list
 */
void List::insertBack(List * _List) {
    // Einf�gen einer vorhandenen Liste am Anfang
    /*
    Die einzuf�genden Knoten werden �bernommen (nicht kopiert)
    Die einzuf�gende Liste _List ist anschlie�end leer.
    Es darf keine Schleife und kein new benutzt werden.
    */

    // Attach _List's last to this' first
    this->head_tail->next->prev = _List->head_tail->prev;
    _List->head_tail->prev->next = this->head_tail->next;


    // Attach _List's first to this' head_tail
    this->head_tail->next = _List->head_tail->next;
    _List->head_tail->next->prev = this->head_tail;


    // Detach _List's head_tail
    _List->head_tail->next = _List->head_tail;
    _List->head_tail->prev = _List->head_tail;

    this->list_size += _List->list_size;
    _List->list_size = 0;
/*
	Es wird ein Objekt �bergeben in dem Knoten vorhanden sein k�nnen.
	Diese Knoten (koplette Kette) werden an den Anfang der Liste (this) �bertragen ohne sie zu kopieren!
*/
}


bool List::getFront(int & key) {
	// entnehmen des Knotens am Anfang der Liste
	// der Wert wird als Parameter zur�ckgegeben
	// der Knoten wird entnommen

	// return false if have nothing to return
	if (list_size == 0)
	    return false;

	key = this->head_tail->next->key;

	// Attach 2nd to head tail
    Node * new_first = this->head_tail->next->next;
    new_first->prev = this->head_tail;

    // delete old first
	delete this->head_tail->next;

	// Attach head tail to new first
	head_tail->next = new_first;

	list_size--;

	// return true if success
	return true;

/*
	Der Wert des vorderen Schl�sselknotens wird r�ckgegeben und der Knoten gel�scht.
	Die Methode del(key) darf nicht zum l�schen benutzt werden.
*/
	return false;
}


bool List::getBack(int & key) {
    // entnehmen des Knotens am Ende der Liste
	// der Wert wird als Parameter zur�ckgegeben
	// der Knoten wird entnommen

    // return false if have nothing to return
    if (list_size == 0)
        return false;

    key = this->head_tail->prev->key;

    // Attach 2nd last to head tail
    Node * new_last = this->head_tail->prev->prev;
    new_last->next = this->head_tail;

    // delete old last
    delete this->head_tail->prev;

    // Attach head tail to new last
    head_tail->prev= new_last;

    list_size--;

    // return true if success
    return true;

/*
	Der Wert des letzten Schl�sselknotens wird r�ckgegeben und der Knoten gel�scht.
	Die Methode del(key) darf nicht zum l�schen benutzt werden.
*/
	return false;
}


bool List::del(int key) {
	// L�schen eines gegebenen Knotens

	Node * target = this->find_node(key);

	// return false if target not found
	if (target == nullptr)
	    return false;
	else {
	    // link left to right
	    target->prev->next = target->next;

	    // link right to left
	    target->next->prev = target->prev;

	    // delete this node
	    delete target;
	    list_size--;
	}
/*
	L�schen des Knotens mit dem Schl�ssel key
*/
    // return true if deleted success
	return true;
}


bool List::search(int key) {
	// suchen eines Knotens

    Node *target = this->find_node(key);

    // if target == null => key not found
    return (target != nullptr);
}


/**
 * Find the first node contain key
 * @param key They key to search with
 * @return The first node contain that key
 */
Node * List::find_node(int key) {

    // return null if empty list
    if (this->list_size == 0)
        return nullptr;

    // search for the target
    Node *target = this->head_tail->next;

    while(target != this->head_tail) {
        if (target->key == key) {
            break;
        }

        target = target->next;
    }

    // return null if key not found
    if (target == head_tail) {
        return nullptr;
    } else {
        return target;
    }
}


bool List::swap(int key1, int key2) {
	// Vertauschen von zwei Knoten
	// Dabei werden die Zeiger der Knoten und deren Nachbarn ver�ndert.

	Node * node1 = find_node(key1);
	Node * node2 = find_node(key2);

	// return false if one of these two not found
	if ((node1 == nullptr) || (node2 == nullptr))
	    return false;

	if (node1 == node2) {
	    return true;
	}

	// if 2 nodes are adjacent
	if ((node1->next == node2) || (node2->next == node1)) {

	    Node * node_left = (node1->next == node2) ? node1 : node2;
	    Node * node_right = (node1->prev == node2) ? node1 : node2;

	    // Reattach left & right to adjacent elems
	    node_left->next = node_right->next;
	    node_right->prev = node_left->prev;

	    // Reattach adjacent elems to left & right
	    node_left->next->prev = node_left;
	    node_right->prev->next = node_right;

	    // Swap left & right
	    node_left->prev = node_right;
	    node_right->next = node_left;
	    return true;
	}


	// swap these 2 nodes' right
	Node * temp = node1->next;
	node1->next = node2->next;
	node2->next = temp;


    // swap these 2 nodes' left
	temp = node1->prev;
	node1->prev = node2->prev;
	node2->prev = temp;

	// reattach rights and lefts of these 2
	node1->next->prev = node1;
	node1->prev->next = node1;
	node2->next->prev = node2;
	node2->prev->next = node2;

/*
	Vertauschen von zwei Knoten mit dem key1 und dem key2
	Es d�rfen nicht nur einfach die Schl�ssel in den Knoten getauscht werden!
	Die Knoten sind in der Kette umzuh�ngen.
*/
    // return true if swap success
	return true;
}


int List::size() const {
	// R�ckgabe der Knoten in der Liste mit O(1)
	return list_size;
}


bool List::test(void) {
	// Testmethode: die Methode durchl�uft die Liste vom Anfang bis zum Ende und zur�ck
	// Es werden dabei die Anzahl der Knoten gez�hlt.
	// Stimmt die Anzahl der Knoten �berein liefert die Methode true
	Node * tmp = head_tail->next;
	int i_next = 0, i_prev = 0;
	while (tmp != head_tail) {
		tmp = tmp->next;
		if (i_next > list_size) return false;
		i_next++;
	}
	if (i_next != list_size) return false;
	tmp = head_tail->prev;
	while (tmp != head_tail) {
		tmp = tmp->prev;
		if (i_prev > list_size) return false;
		i_prev++;
	}
	return i_prev == i_next;
}


List & List::operator = (const List & _List) {
	// in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
	// Kopiert wird in das Objekt "this"
	if (this == &_List) return *this;		//  !! keine Aktion notwendig
	list_form = _List.list_form;
	Node * tmp_node;
	if (list_size) {
		Node * tmp_del;
		tmp_node = head_tail->next;
		while (tmp_node != head_tail)		// Alle eventuell vorhandenen Knoten in this l�schen
		{
			tmp_del = tmp_node;
			tmp_node = tmp_node->next;
			delete tmp_del;
		}
		list_size = 0;
		head_tail->next = head_tail;
		head_tail->prev = head_tail;
	}
	tmp_node = _List.head_tail->next;		// Die Listen-Knotenwerte werden kopiert
	while (tmp_node != _List.head_tail) {
		insertBack(tmp_node->key);
		tmp_node = tmp_node->next;
	}
	if (_List.temp) delete & _List;			// ist die �bergebene Liste eine tempor�re Liste? -> aus Operator +
	return *this;
}


List & List::operator = (const List * _List) {
	// in dem Objekt _List sind die Knotenwerte enthalten, die Kopiert werden sollen.
	// Kopiert wird in das Objekt "this"
	if (this == _List) return *this;		//  !! keine Aktion notwendig
	list_form = _List->list_form;
	Node * tmp_node;
	if (list_size) {
		Node * tmp_del;
		tmp_node = head_tail->next;
		while (tmp_node != head_tail)		// Alle eventuell vorhandenen Knoten in this l�schen
		{
			tmp_del = tmp_node;
			tmp_node = tmp_node->next;
			delete tmp_del;
		}
		list_size = 0;
		head_tail->next = head_tail;
		head_tail->prev = head_tail;
	}
	tmp_node = _List->head_tail->next;
	while (tmp_node != _List->head_tail)	// Die Listen-Knotenwerte werden kopiert
	{
		insertBack(tmp_node->key);
		tmp_node = tmp_node->next;
	}
	if (_List->temp) delete _List;			// ist die �bergebene Liste eine tempor�re Liste? -> aus Operator +
	return *this;
}


List & List::operator + (const List & List_Append) {
	// Die Methode +
	// Es werden zwei Listen aneinander gehangen.
	// Dabei werden beide Ursprungslisten nicht ver�ndert. Es entsteht eine neue Ergebnisliste.
	Node * tmp_node;
	List * tmp;
	if (temp) {										// this ist eine tempor�re Liste und kann ver�ndert werden
		tmp = this;
	}
	else {	
		tmp = new List(this);						// this ist keine tempor�re Liste -> Kopie erzeugen
		tmp->temp = true;							// Merker setzten, dass es sich um eine tempor�re Liste handelt
	}
	if (List_Append.list_size) {					// anh�ngen der �bergebenen Liste an tmp
		tmp_node = List_Append.head_tail->next;
		while (tmp_node != List_Append.head_tail) {
			tmp->insertBack(tmp_node->key);
			tmp_node = tmp_node->next;
		}
	}
	if (List_Append.temp) delete & List_Append;		// wurde eine tempor�re Liste �bergeben, dann wird diese gel�scht						
	return *tmp;
}


List & List::operator + (const List * List_Append) {
	// Die Methode +
	// Es werden zwei Listen aneinander gehangen.
	// Dabei werden beide Ursprungslisten nicht ver�ndert. Es entsteht eine neue Ergebnisliste.
	Node * tmp_node;
	List * tmp;
	if (temp) {										// this ist eine tempor�re Liste und kann ver�ndert werden
		tmp = this;
	}
	else {
		tmp = new List(this);						// this ist keine tempor�re Liste -> Kopie erzeugen
		tmp->temp = true;							// Merker setzten, dass es sich um eine tempor�re Liste handelt
	}
	if (List_Append->list_size) {					// anh�ngen der �bergebenen Liste an tmp
		tmp_node = List_Append->head_tail->next;
		while (tmp_node != List_Append->head_tail) {
			tmp->insertBack(tmp_node->key);
			tmp_node = tmp_node->next;
		}
	}
	if (List_Append->temp) delete List_Append;		// wurde eine tempor�re Liste �bergeben, dann wird diese gel�scht					
	return *tmp;
}


void List::format(const std::string & start, const std::string & zwischen, const std::string & ende) {
	// Setzen des Formates f�r die Ausgabesteuerung der Liste bei cout
	// das Format wird f�r den �berladenen Operator << verwendet
	list_form.start = start;
	list_form.zwischen = zwischen;
	list_form.ende = ende;
}


std::ostream & operator<<(std::ostream  & stream, List const & Liste) {
	// Ausgabe der Liste mit cout
	stream << Liste.list_form.start;

	// Print empty list
	if (Liste.size() == 0) {
	    stream << Liste.list_form.ende;
	    return stream;
	}

	for (Node * tmp = Liste.head_tail->next; tmp != Liste.head_tail; tmp = tmp->next)
		stream << tmp->key << (tmp->next == Liste.head_tail ? Liste.list_form.ende : Liste.list_form.zwischen);
	if (Liste.temp) delete & Liste;			// wurde eine tempor�re Liste �bergeben, dann wird diese gel�scht
	return stream;
}


std::ostream & operator<< (std::ostream & stream, List const * Liste) {
	// Ausgabe der Liste mit cout
	stream << Liste->list_form.start;

    // Print empty list
    if (Liste->size() == 0) {
        stream << Liste->list_form.ende;
        return stream;
    }


    for (Node * tmp = Liste->head_tail->next; tmp != Liste->head_tail; tmp = tmp->next)
		stream << tmp->key << (tmp->next == Liste->head_tail ? Liste->list_form.ende : Liste->list_form.zwischen);
	if (Liste->temp) delete Liste;			// wurde eine tempor�re Liste �bergeben, dann wird diese gel�scht
	return stream;
}
