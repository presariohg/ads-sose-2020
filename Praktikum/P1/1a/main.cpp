#include <iostream>
#define CATCH_CONFIG_RUNNER
#include "catch.hpp"
#include "List.h"
using namespace std;

//int main() {
//    List list;
////    list.insertFront(2);
////    list.insertFront(1);
//    list.insertFront(5);
//    list.insertFront(7);
//
//    list.insertBack(9);
//    list.insertBack(4);
//
//
//    List list2;
//    list2.insertFront(3);
//    list2.insertBack(3);
//    list2.insertBack(3);
//    list2.insertBack(3);
//    list.insertBack(list2);
//
//    cout << list2;
//    cout << list2.size() << "\n";
//
////    int a;
//
//    cout << list;
//    cout << list.size() << "\n\n";
//
//    cout << list.search(5) << "\n";
//
//    cout << list;
//    cout << list.size() << "\n\n";
//
////    list.del(3);
////    list.del(4);
////    list.swap(1,2);
//
//    cout << list;
//    cout << list.size() << "\n";
//
//    cout << list.test();
//    return 0;
//}

int main() {
	int result = Catch::Session().run();
	int i;
	List MyList;

	for (i = 0; i < 10; i++) {
		MyList.insertFront(i + 1);
		MyList.insertBack(i + 100);
	}

	cout << MyList;

	cout << "100: " << (MyList.search(100) ? "gefunden" : "nicht gefunden") << endl;
	cout << "99: " << (MyList.search(99) ? "gefunden" : "nicht gefunden") << endl << endl;

	while (MyList.getBack(i))
		cout << i << " ";
	cout << endl << endl;

	List MyList1, MyList2, MyList3;
	List * MyList_dyn = new List;

	for (i = 0; i < 10; i++) {
		MyList1.insertFront(i + 1);
		MyList2.insertBack(i + 100);
	}

	MyList1.format("MyList1\n<<", ", ", ">>\n\n");
	cout << MyList1;

	MyList_dyn->format("MyList_dyn\n<<", ", ", ">>\n\n");
	MyList_dyn->insertFront(-111);
	cout << MyList_dyn;

	MyList2.format("MyList2\n<<", ", ", ">>\n\n");
	cout << MyList2;

	MyList3 = MyList1 + MyList_dyn + MyList2;

	delete MyList_dyn;

	cout << "Groesse von MyList3: " << MyList3.size() << endl << endl;

	MyList3.format("MyList3\n<<", ", ", ">>\n\n");
	cout << MyList3 << endl;

	MyList3.swap(8, 103);
	MyList3.swap(100, -111);

	cout << MyList3;

	if (MyList3.test())
		cout << "MyList3: Zeiger OK\n\n";
	else
		cout << "MyList3: Zeiger ******Error\n\n";

	return 0;
}
