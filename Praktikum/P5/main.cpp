#include <iostream>
#include "MyGraph.h"

int main() {
//    std::vector<int> nodeList = {7, 10, 0, 1, 3, 1, 1, 2, 6, 3, 2, 4, 5, 1, 4, 4, 3, 2, 3, 5, 5, 6};
//    std::vector<int> nodeList = {6, 9, 0, 2, 0, 1, 0, 5, 2, 1, 2, 3, 2, 4, 1, 0, 3, 5, 3, 4};

//    MyGraph graph(nodeList);

    std::map<int, std::vector<int>> table;
    table[0] = {4};
    table[1] = {2};
    table[2] = {1, 4, 6};
    table[3] = {1, 2, 5};
    table[4] = {4};
    table[5] = {1, 6};
    table[6] = {0, 3};

    MyGraph graph;

    graph.setAdjacentList(table);

    for (std::pair<int, std::vector<int>> pair : graph.getAdjacentList()) {
        std::cout << pair.first << ": ";

        for (int neighbor : pair.second)
            std::cout << neighbor << " , ";

        std::cout << "\n";
    }

    std::cout << "\n";

    std::vector<int> path = graph.startTraverseRecursiveDFS(table);
    for (int i = 0 ; i < path.size(); i++) {
        std::cout << i << " : " << path[i] << "\n";
    }

    std::cout << "\n";

    path = graph.iterativeDFS(table, 3);
    for (int i = 0 ; i < path.size(); i++) {
        std::cout << i << " : " << path[i] << "\n";
    }

    return 0;
}
