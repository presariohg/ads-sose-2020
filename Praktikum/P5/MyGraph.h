//
// Created by presariohg on 23/06/2020.
//

#ifndef P5_MYGRAPH_H
#define P5_MYGRAPH_H
#include <vector>
#include <map>
#include <stack>
#include <queue>

#define DEAD_END -1

class MyGraph {

private:
    int V; // amount vertices
    int E; // amount edges

    std::vector<int> nodeList;
    std::map<int, std::vector<int>> adjacentList;

    std::vector<bool> marked;
    std::vector<int> edgeTo;

    /**
     * @param node current node
     * @param edgeTo path to current node
     */
    void recursiveTraverseDFS(std::map<int, std::vector<int>> adjacentList, int node, std::vector<int> &edgeTo) {
        std::vector<int> nextNodes = adjacentList[node];
        marked[node] = true;

        for (int nextNode : nextNodes) {
            // skip dead ends
            if (nextNode == DEAD_END)
                continue;

            if (!marked[nextNode]) {
                edgeTo[nextNode] = node;
                recursiveTraverseDFS(adjacentList, nextNode, edgeTo);
            }
        }

    }

    static std::map<int, std::vector<int>> toAdjacentList(std::vector<int> nodeList) {
        int amountNode = nodeList[0];
        int amountEdge = nodeList[1];

        std::map<int, std::vector<int>> adjacentList;
        std::vector<bool> appeared;

        appeared.resize(amountNode);
        for (bool && i : appeared)
            i = false;


        adjacentList.clear();

        for (int i = 1; i <= amountEdge; i++) {
            int nodeStart = nodeList[2 * i];
            int nodeEnd = nodeList[2 * i + 1
                                   ];
            adjacentList[nodeStart].push_back(nodeEnd);

            appeared[nodeStart] = true;
        }

        for (int node = 0; node < appeared.size(); node++) {
            if (!appeared[node])
                adjacentList[node].push_back(DEAD_END);
        }

        return adjacentList;
    }

public:
    MyGraph() = default;

    MyGraph(std::vector<int> nodeList) {
        this->nodeList = nodeList;
        this->adjacentList = MyGraph::toAdjacentList(nodeList);
    }

    void setNodeList(std::vector<int> &newNodeList) {
        this->nodeList = newNodeList;
    }

    void setAdjacentList(std::map<int, std::vector<int>> table) {
        this->adjacentList = table;
    }

    std::map<int, std::vector<int>> getAdjacentList() {
        return adjacentList;
    }

    std::vector<int> startTraverseRecursiveDFS(std::map<int, std::vector<int>> adjacentList) {
        std::vector<int> path = {};
        this->marked.clear();

        for (int i = 0; i < adjacentList.size(); i++) {
            this->marked.push_back(false);
            path.push_back(-1);
        }


        this->recursiveTraverseDFS(adjacentList, 3, path);
        return path;
    }

    std::vector<int> iterativeDFS(std::map<int, std::vector<int>> adjacentList, int startNode) {
        std::vector<int> path = {};
        this->marked.clear();

        for (int i = 0; i < adjacentList.size(); i++) {
            this->marked.push_back(false);
            path.push_back(-1);
        }

        std::queue<int> queue;
        queue.push(startNode);

        while (!queue.empty()) {
            int thisNode = queue.front();
            queue.pop();

            if (thisNode == DEAD_END)
                continue;

            this->marked[thisNode] = true;

            for (int nextNode : adjacentList[thisNode])
                if (!marked[nextNode]){
                    queue.push(nextNode);
                    path[nextNode] = thisNode;
                }
        }

        return path;
    }

    std::vector<int> iterativeBFS(std::map<int, std::vector<int>> adjacentList, int startNode) {
        return {};
    }

    std::vector<int> connectedComponents(std::map<int, std::vector<int>> adjacentList) {
        return {};
    }
};


#endif //P5_MYGRAPH_H
