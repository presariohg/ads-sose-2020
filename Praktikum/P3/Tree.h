/*************************************************
* ADS Praktikum 2.2
* Tree.h
* Erweiterung um Hilfsattribute und -funktionen gestattet, wenn erforderlich.
*************************************************/
#pragma once
#include <string>
#include "TreeNode.h"
#include "catch.h"
#include <queue>

using namespace std;

class Tree {

private:
    ///////////////////////////////////////
    // Ihr Code hier:

    // Initialize the nil node.
    TreeNode * anker;


    int nodeIDCounter;


    int nodeCounter;


    static int nodePosID(int alter, int plz, double einkommen);


    void preOrderPrint(TreeNode * node) const;


    bool isNameFound(const std::string & name, TreeNode * node) const;


    TreeNode * searchNode(const int &nodePosID, TreeNode * node) const;


    bool rotateTreeRight(TreeNode * parent, TreeNode * leftChild);


    bool rotateTreeLeft(TreeNode * parent, TreeNode * rightChild);


    void validate(TreeNode * node);


    static bool split4Node(TreeNode * node);


    static bool split3Node(TreeNode * node);


    static bool split2Node(TreeNode * node);

    //
    ////////////////////////////////////

public:
    ///////////////////////////////////////
    // Ihr Code hier:

    Tree();


    void addNode(std::string name, int alter, double einkommen, int plz);


    bool searchNode(const std::string & name) const;


    void printAll() const;


    void printLevelOrder() const;


    //
    ////////////////////////////////////
    // friend-Funktionen sind f�r die Tests erforderlich und m�ssen unangetastet bleiben!
    friend TreeNode * get_anker(Tree& TN);
};

bool isNull(TreeNode * node);