/*************************************************
* ADS Praktikum 2.2
* Tree.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "Tree.h"
#include "TreeNode.h"
#include <iostream>
#include <iomanip>

using namespace std;

////////////////////////////////////
// Ihr Code hier:


Tree::Tree() {
    this->anker = nullptr;
    this->nodeIDCounter = 0;
    this->nodeCounter = 0;
}


int Tree::nodePosID(int alter, int plz, double einkommen) {
    return alter + plz + einkommen;
}


void Tree::addNode(std::string name, int alter, double einkommen, int plz) {

    int nodeID = ++this->nodeIDCounter;
    int nodePosID = this->nodePosID(alter, plz, einkommen);
    auto * newNode = new TreeNode(nodePosID, nodeID, name, alter, einkommen, plz);
    newNode->setLeft(nullptr);
    newNode->setRight(nullptr);

    if (this->nodeCounter == 0) { // if empty tree
        newNode->setParent(nullptr);
        newNode->setColor(BLACK);
        this->anker = newNode;

    } else { // else already had a root, find a node to attach new node in
        newNode->setColor(RED);

        bool parentFound = false;
        TreeNode * parent = this->anker;
        while (!parentFound) {
            if (*newNode < *parent)  // if new node < this node, turn left
                if (isNull(parent->getLeft())) { // if left empty, insert there
                    parent->setLeft(newNode);
                    newNode->setParent(parent);
                    parentFound = true;
                } else {
                    parent = parent->getLeft();
                    continue;
                }

            else // else new node >= this node, turn right
                if (isNull(parent->getRight())) { // if right empty, insert there
                    parent->setRight(newNode);
                    newNode->setParent(parent);
                    parentFound = true;
                } else {
                    parent = parent->getRight();
                    continue;
                }
        }
//        this->insertNodeRecursive(newNode, anker);
    }

    this->validate(newNode);

    this->nodeCounter++;
}


/**
 * Validate this red black tree if it violates any red black tree rules, then fix it
 */
void Tree::validate(TreeNode * node) {
    // omit null nodes
    if (isNull(node))
        return;

    // case 1: red root
    if (node == this->anker) {
        if (node->colorIs(RED)) {
            node->setColor(BLACK); // simply set it to black
            return;
        } else // else black root, no problem, return
            return;
    }

    TreeNode * parent = node->getParent();
    TreeNode * grandparent = node->getGrandparent();

    if (isNull(grandparent)) //special cases, since not allowed to use NIL node anymore
        return;


    if (isNull(parent))
        return;


    // parent cannot be null (only root has null parent, and root nodes should not end up here)
    // grandparent cannot be null neither (only children of root has null grandparent, and they should not end up here)

    TreeNode * uncle  = node->getUncle();
    const bool RED_UNCLE = (isNull(uncle)) ? false : uncle->colorIs(RED);
    const bool BLACK_UNCLE = (isNull(uncle)) ? true : uncle->colorIs(BLACK);

    // case 2 & 3: red parent black uncle
    if (parent->colorIs(RED) &&
        BLACK_UNCLE) {

        // case 2a: right line
        if (node->isRightChild() &&
            parent->isRightChild()) {

            // recolor parent, grandparent
            parent->recolor();
            grandparent->recolor();

            // rotate grandparent in the opposite direction: left
            this->rotateTreeLeft(grandparent, grandparent->getRight());

            // validate to check if the rotation violates anything
            validate(grandparent);

        } else // case 2b: left line

        if (node->isLeftChild() &&
            parent->isLeftChild()) {

            // recolor parent, grandparent
            parent->recolor();
            grandparent->recolor();

            // rotate grandparent in the opposite direction: right
            this->rotateTreeRight(grandparent, grandparent->getLeft());

            // validate to check if the rotation violates anything
            validate(grandparent);

        } else // case 3a: right v

        if (node->isLeftChild() &&
            parent->isRightChild()) {

            // rotate parent in this direction
            this->rotateTreeRight(parent, parent->getLeft());

            // validate to check if the rotation violates anything
            validate(parent);

        } else // case 3b: left v

        if (node->isRightChild() &&
            parent->isLeftChild()) {

            // rotate parent in this direction
            this->rotateTreeLeft(parent, parent->getRight());

            // validate to check if the rotation violates anything
            validate(parent);
        }

        return;
    }

    // case 4: red parent red uncle
    if (parent->colorIs(RED) &&
        RED_UNCLE) {
        // recolor uncle, parent, grandparent
        // if uncle is red then it cannot be null. now uncle, parent and grandparent are null safe.
        uncle->recolor();
        parent->recolor();
        grandparent->recolor();

        validate(grandparent);
    }
}


/**
 * Search a node given its name.
 * @param name The name to search
 * @return True if found the node match given name, otherwise false
 */
bool Tree::searchNode(const std::string & name) const {
    return isNameFound(name, this->anker);
}


TreeNode * Tree::searchNode(const int& nodePosID, TreeNode * node) const {
    if (isNull(node)) // reached the end of this branch
        return nullptr;

    if (node->getNodePosID() == nodePosID)
        return node;
    else if (node->getNodePosID() < nodePosID)
        return this->searchNode(nodePosID, node->getRight());
    else
        return this->searchNode(nodePosID, node->getLeft());
}


/**
 * Recursively find a node given a name in the tree.
 * @param name The node's name
 * @param node Current node. Start at anchor.
 * @return True if found the node match given name, otherwise false
 */
bool Tree::isNameFound(const std::string & name, TreeNode * node) const {

    if (isNull(node))
        return false; //empty nodes are always false

    if (node->getName() == name) {
        std::cout << "Record ID: " << node->getNodeID();
        std::cout << ", Name: " << node->getName();
        std::cout << ", Age: " << node->getAlter();
        std::cout << ", Income: " << node->getEinkommen();
        std::cout << ", PLZ: " << node->getPLZ();
        std::cout << ", PosID: " << node->getNodePosID();
        std::cout << "\n";

        return true;
    } else {
        if (isNameFound(name, node->getLeft()))
            return true; //if node found in left side, don't need to search in the right side
        else
            return isNameFound(name, node->getRight());
    }
}


/**
 * @return True if node is black and has 2 red children. Otherwise false
 */
bool Tree::split4Node(TreeNode *node) {
    if (node->colorIs(RED))
        return false;

    if (isNull(node->getRight()))
        return false;

    if (isNull(node->getLeft()))
        return false;

    return node->colorIs(BLACK) &&
           node->getRight()->colorIs(RED) &&
           node->getLeft()->colorIs(RED);
}


/**
 * @return True if node is black and has 1 red children. Otherwise false
 */
bool Tree::split3Node(TreeNode *node) {
    if (node->colorIs(RED))
        return false;

    // if red left child && black right child
    if (!isNull(node->getLeft()) &&
        node->getLeft()->colorIs(RED))
        if (isNull(node->getRight()) || // null is black
            node->getRight()->colorIs(BLACK))
            return true;

    // if red left child && black right child
    if (!isNull(node->getRight()) &&
        node->getRight()->colorIs(RED))
        if (isNull(node->getLeft()) || // null is black
            node->getLeft()->colorIs(BLACK))
            return true;

    return false;
}


/**
 * @return True if node is black and has 0 red children. Otherwise false
 */
bool Tree::split2Node(TreeNode *node) {
    if (node->colorIs(RED))
        return false;

    if (isNull(node->getLeft())) {
        if (isNull(node->getRight())) // null always black
            return true;
        else {
            return (node->getRight()->colorIs(BLACK));
        }
    }

    if (isNull(node->getRight())) {
        if (isNull(node->getLeft())) // null always black
            return true;
        else {
            return (node->getLeft()->colorIs(BLACK));
        }
    }

    return node->getRight()->colorIs(BLACK) &&
           node->getLeft()->colorIs(BLACK);

}


/**
 * @param parent Rotate this node on the tree to the left
 * @param rightChild Parent node's left child
 * @return True if successful. Otherwise false, the tree unchanged
 */
bool Tree::rotateTreeLeft(TreeNode *parent, TreeNode *rightChild) {
    // catch all invalid inputs
    if (isNull(rightChild) ||
        isNull(parent))
        return false;

    if (parent->getRight() != rightChild)
        return false;

    // change root if needed
    if (this->anker == parent)
        this->anker = parent->getRight();

    parent->rotateLeft();
    return true;
}

/**
 * @param parent Rotate this node on the tree to the right
 * @param leftChild Parent node's left child
 * @return True if successful. Otherwise false, the tree unchanged
 */
bool Tree::rotateTreeRight(TreeNode *parent, TreeNode *leftChild) {
    // catch all invalid inputs
    if (isNull(leftChild) ||
        isNull(parent))
        return false;

    if (parent->getLeft() != leftChild)
        return false;

    // change root if needed
    if (this->anker == parent)
        this->anker = parent->getLeft();

    parent->rotateRight();


    return true;
}


void Tree::printAll() const {
    std::cout << "Currently database holds " << this->nodeCounter << " records.\n";
    std::cout << "ID    | Name                 | Age |     Income |   PLZ |    Pos |   Color \n";
    std::cout << "======|======================|=====|============|=======|========|=========\n";

    this->preOrderPrint(this->anker);
}


void Tree::preOrderPrint(TreeNode *node) const {
    if (isNull(node)) {
        return;
    }

    node->print();

    preOrderPrint(node->getLeft());

    preOrderPrint(node->getRight());

}


void Tree::printLevelOrder() const {
    // Enqueue all black nodes in level order

    // First, traverse all nodes in level order:
    std::queue<TreeNode *> queueAll;
    std::queue<TreeNode *> queueBlack;
    std::queue<int> queueHeight;

    queueAll.push(this->anker);

    TreeNode * node;

    while (!queueAll.empty()) {
        node = queueAll.front();
        queueAll.pop();

        if (!isNull(node->getLeft())) {
            queueAll.push(node->getLeft());
        }

        if (!isNull(node->getRight())) {
            queueAll.push(node->getRight());
        }

        // push black the red's children first, to avoid the case "red left black right children".
        if (node->colorIs(RED)) {
            if (!isNull(node->getLeft())) {
                queueBlack.push(node->getLeft());
                queueHeight.push(node->getLeft()->blackHeight());
            }

            if (!isNull(node->getRight())) {
                queueBlack.push(node->getRight());
                queueHeight.push(node->getRight()->blackHeight());
            }
        }

        // only push black if parent is black, or else these nodes will be pushed twice
        if (node->colorIs(BLACK))
            if (!isNull(node->getParent())) {
                if (node->getParent()->colorIs(BLACK)) {
                    queueBlack.push(node);
                    queueHeight.push(node->blackHeight());
                }
            } else { // else it must be the root
                queueBlack.push(node);
                queueHeight.push(node->blackHeight());
            }
    }



    std::cout << "\nLevel 0: ";

    int prevHeight = queueHeight.front();

    while (!queueBlack.empty()) {
        node = queueBlack.front();
        queueBlack.pop();

        int currentHeight = queueHeight.front();
        queueHeight.pop();

        if (currentHeight != prevHeight) {
            std::cout << "\nLevel " << currentHeight <<": ";
        } // new line with each level

        prevHeight = currentHeight;

        if (this->split4Node(node)) { // node has 2 red children
            std::cout << "( " << node->getLeft()->getNodePosID();
            std::cout << " | " << node->getNodePosID();
            std::cout << " | " << node->getRight()->getNodePosID() << " )  ";
        }

        if (this->split3Node(node)) {
            if (!isNull(node->getLeft()) && // if red left child
                node->getLeft()->colorIs(RED)) {

                std::cout << "( " << node->getLeft()->getNodePosID();
                std::cout << " | " << node->getNodePosID() << " )  ";

            } else { // else red right child
                std::cout << "( " << node->getNodePosID();
                std::cout << " | " << node->getRight()->getNodePosID() << " )  ";
            }
        }

        if (this->split2Node(node))
            std::cout << "( " << node->getNodePosID() << " )  ";
    }

    std::cout << "\n\n";
}


//
////////////////////////////////////