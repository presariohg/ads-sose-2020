/*************************************************
* ADS Praktikum 2.1
* RingNode.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "RingNode.h"
#include "NodeException.h"
#include <iostream>

// Ihr Code hier:


/**
 * Initialize a new blank Node
 */
RingNode::RingNode() {
    this->isAgeSet = false;
    this->isDataSet = false;
    this->isDescriptionSet = false;
    this->isNextSet = false;

    this->setNext(nullptr);
}


/**
 * Initialize a new node with age, description and data
 * @param age This node's age. 0 would the newest node, 5 would be the oldest
 * @param description This node's description
 * @param data This node's symbolic data
 */
RingNode::RingNode(int age, std::string description, std::string data) {
    this->setAge(age);
    this->setDescription(description);
    this->setData(data);
    this->isNextSet = false;

    this->setNext(nullptr);
}


/**
 * @return True if this Node is valid (has next, age and data set)
 */
bool RingNode::isValid() const {
    return isNextSet && isAgeSet && isDataSet && isDescriptionSet;
}


/**
 * @return This node's data
 */
std::string RingNode::getData() const {
    if (!this->isDataSet)
        throw NodeException::BlankDataException();

    return this->symbolicData;
}


/**
 * Set new data to this node
 * @param data New data to set
 */
void RingNode::setData(std::string data) {
    this->symbolicData = data;
    this->isDataSet = true;
}


/**
 * @return This node's age
 */
int RingNode::getAge() const {
    if (!this->isAgeSet)
        throw NodeException::AgeNotSetException();

    return this->oldAge;
}


/**
 * Set new age to this node
 * @param age New age to set
 */
void RingNode::setAge(int age) {
    this->oldAge = age;
    this->isAgeSet = true;
}


/**
 * @return This node's decription
 */
std::string RingNode::getDescription() const{
    if (!this->isDescriptionSet)
        throw NodeException::BlankDescriptionException();

    return this->description;
}


/**
 * Set this node's description
 * @param description New description to set.
 */
void RingNode::setDescription(std::string description) {
    this->description = description;
    this->isDescriptionSet = true;
}


/**
 * @return This node's next node.
 */
RingNode * RingNode::getNext() const {
    return this->next;
}


/**
 * Set this node's next node
 * @param next The node after this node in ring.
 */
void * RingNode::setNext(RingNode * next) {
    this->next = next;
    this->isNextSet = true;
}


std::ostream& operator<< (std::ostream & out, const RingNode & node) {
    out << "Age: " << node.getAge();
    out << "\nDescription: " << node.getDescription();
    out << "\nData: " << node.getData();

    out << "\n--------------------------\n";
    return out;
}

std::ostream& operator<< (std::ostream & out, const RingNode * node) {
    return out << *node;
}
//
////////////////////////////////////