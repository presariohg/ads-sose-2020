/*************************************************
* ADS Praktikum 2.1
* Ring.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
// Ring .cpp
#include "Ring.h"
#include <iostream>

// Ihr Code hier:

const int MAX_NODE = 6;


Ring::Ring() {
    this->AnzahlNodes = 0;

    this->anker = new RingNode();

    this->anker->setNext(this->anker);
    this->anker->setAge(0);
}


/**
 * Overwrite new backup at the oldest node if reached max number of backup, otherwise continue on the ring
 * @param description
 * @param data
 */
void Ring::addNode(std::string description, std::string data) {
    // increase other node's age
    RingNode * lastNode;
    for (lastNode = anker; lastNode->getNext() != anker; lastNode = lastNode->getNext())
        lastNode->setAge(lastNode->getAge() + 1);

    lastNode->setAge(lastNode->getAge() + 1);

    // if ring is full, overwrite the oldest node (anchor -> next)
    if (this->AnzahlNodes == MAX_NODE) {

        lastNode = this->anker->getNext();

        lastNode->setAge(0);
        lastNode->setData(data);
        lastNode->setDescription(description);

        // move the anchor
        this->anker = lastNode;

    } else if (this->AnzahlNodes == 0) { // if ring is empty, overwrite the anchor

        this->anker->setAge(0);
        this->anker->setData(data);
        this->anker->setDescription(description);

    } else { // else neither empty nor full, insert a new node after anchor

        RingNode *newNode = new RingNode();
        newNode->setAge(0);
        newNode->setData(data);
        newNode->setDescription(description);
        newNode->setNext(this->anker->getNext());
        this->anker->setNext(newNode);

        // move the anchor
        this->anker = newNode;
    }

    if (this->AnzahlNodes < MAX_NODE)
        this->AnzahlNodes++;

}



bool Ring::search(std::string keyword) const {

    RingNode * node = this->anker;
    for (int i = 0; i < MAX_NODE; i++) {

        // skip invalid nodes
        if (!node->isValid()) {
            node = node->getNext();
            continue;
        }

        // if found keyword in this node's data
        if (!node->getData().compare(keyword)) { //compare == 0 <=> 2 strings are the same
            std::cout << "Found in this backup:\n";
            std::cout << node;
            return true;
        }
        node = node->getNext();
    }

    return false;
}


/**
 * Print all stored backups
 */
void Ring::print() const {
    std::cout << "Currently storing " << this->AnzahlNodes << " backup(s):\n\n";

    // Using a On^2 algorithm sucks, but there's no other way with a simple linked list. Otherwise we just
    // have to compromise: printing the oldest backup first, ascend to the newest.

    int currentAge = 0;
    for (int i = 0; i < this->AnzahlNodes; i++) {
        RingNode *node = this->anker;
        for (int j = 0; j < this->AnzahlNodes; j++) {
            if (node->getAge() == currentAge) {
                std::cout << node;
                break;
            }
            node = node->getNext();
        }
        currentAge++;
    }

//    for (node = anker; node->getNext() != anker; node = node->getNext()) {
//        if (node->isValid())
//            std::cout << node;
//
//    }
//
//    if (node->isValid())
//        std::cout << node;

}

//
////////////////////////////////////
