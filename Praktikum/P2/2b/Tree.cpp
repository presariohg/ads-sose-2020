/*************************************************
* ADS Praktikum 2.2
* Tree.cpp
* Erweiterung um Hilfsfunktionen gestattet.
*************************************************/
#include "Tree.h"
#include "TreeNode.h"
#include <iostream>
#include <iomanip>

using namespace std;

////////////////////////////////////
// Ihr Code hier:


Tree::Tree() {
    this->anker = nullptr;
    this->nodeIDCounter = 0;
    this->nodeCounter = 0;
}


int Tree::nodePosID(int alter, int plz, double einkommen) {
    return alter + plz + einkommen;
}


/**
 * Insert a new node using a recursive algorithm. Same complexity but shorter.
 * @param child The new node to insert
 * @param parent Parent node
 */
void Tree::insertNodeRecursive(TreeNode *child, TreeNode *parent) {
    if (child->getNodePosID() < parent->getNodePosID()) { // if new node < this node, turn left

        if (parent->getLeft() == nullptr) { // if left empty, insert there
            parent->setLeft(child);
            child->setParent(parent);
        } else
            insertNodeRecursive(child, parent->getLeft()); // else turn left then repeat

    } else { // else new node >= this node, turn right

        if (parent->getRight() == nullptr) {// if right empty, insert there
            parent->setRight(child);
            child->setParent(parent);
        } else
            insertNodeRecursive(child, parent->getRight()); // else turn right then repeat
    }
}


void Tree::addNode(std::string name, int alter, double einkommen, int plz) {
    if (this->nodeIDCounter == 0) { // if empty tree

        int nodeID = this->nodeIDCounter++;

        int nodePosID = this->nodePosID(alter, plz, einkommen);

        this->anker = new TreeNode(nodePosID, nodeID, name, alter, einkommen, plz);

        this->anker->setLeft(nullptr);
        this->anker->setRight(nullptr);
        this->anker->setParent(nullptr);
    } else { // else already had a root, find which node to attach new node in

        int nodeID = this->nodeIDCounter++;
        int nodePosID = this->nodePosID(alter, plz, einkommen);
        auto * newNode = new TreeNode(nodePosID, nodeID, name, alter, einkommen, plz);
        newNode->setLeft(nullptr);
        newNode->setRight(nullptr);

        this->insertNodeRecursive(newNode, anker);

    }

    this->nodeCounter++;
}


/**
 * Delete a node from the tree.
 * @param nodePosID The node's PosID
 */
bool Tree::deleteNode(int nodePosID) {
    TreeNode * target = searchNode(nodePosID, this->anker);

    if (target == nullptr)
        return false;

    if (target->isEmpty()) { // if this node has no child
        if (target == this->anker) { // if this node is the anchor, simply delete it
            this->anker = nullptr;
            delete target;
        } else { // else it must have a parent. delete it from there.
            TreeNode * targetParent = target->getParent();

            if (target == targetParent->getRight()) // if it's its parent's right child
                targetParent->setRight(nullptr);
            else // else it must be its parent's left child
                targetParent->setLeft(nullptr);

            delete target;
        }


    } else if (!target->isFull()) { //if target has 1 child
        TreeNode * targetChild = (target->getLeft() != nullptr) ? target->getLeft() : target->getRight();

        if (target == this->anker) { // if this node is the anchor, make its child the new anchor then delete it
            this->anker = targetChild;
            this->anker->setParent(nullptr);

            delete target;
        } else { // else it must have a parent. delete it from there.
            TreeNode * targetParent = target->getParent();

            if (target == targetParent->getRight()) // make target's child replace it, then delete it.
                targetParent->setRight(targetChild);
            else
                targetParent->setLeft(targetChild);

            targetChild->setParent(targetParent);

            delete target;
        }

    } else { // else target must have 2 children. things get interesting..
        TreeNode * rightMin = this->findMin(target->getRight()); // since it's a local minimum, it must have at most 1 child

        // replace target's attributes with rightMin
        target->setAlter(rightMin->getAlter());
        target->setEinkommen(rightMin->getEinkommen());
        target->setPLZ(rightMin->getPLZ());
        target->setName(rightMin->getName());
        target->setNodeID(rightMin->getNodeID());

        int newNodePosID = rightMin->getNodePosID();

        // delete old rightMin
        this->deleteNode(rightMin->getNodePosID());

        // delete rightMin before reset target's nodeposid, or the function would delete target once more
        // rather than rightMin
        target->setNodePosID(newNodePosID);
    }

    this->nodeCounter--;

    return true;
}


/**
 * Search a node given its name.
 * @param name The name to search
 * @return True if found the node match given name, otherwise false
 */
bool Tree::searchNode(const std::string & name) const {
    return isNameFound(name, this->anker);
}


TreeNode * Tree::searchNode(const int& nodePosID, TreeNode * node) const {
    if (node == nullptr)
        return nullptr;

    if (node->getNodePosID() == nodePosID)
        return node;
    else if (node->getNodePosID() < nodePosID)
        return this->searchNode(nodePosID, node->getRight());
    else
        return this->searchNode(nodePosID, node->getLeft());
}


/**
 * Find the min node on this subtree
 * @param node This subtree's node
 * @return The node has the lowest nodePosID on this subtree
 */
TreeNode * Tree::findMin(TreeNode *node) const {
    if (node->getLeft() == nullptr)
        return node;
    else
        return this->findMin(node->getLeft());
}


///**
// * Find a node given it's child's PosID, aka find the target's parent node
// * @return Node's parent node
// */
//TreeNode * Tree::searchBranch(const int& nodePosID, TreeNode * node) const {
//    if (node == nullptr)
//        return nullptr;
//
//    if (node->isEmpty()) {
//        return nullptr;
//    }
//
//    // if target found in left, return node.
//    if ((node->getLeft() != nullptr) &&
//        (node->getLeft()->getNodePosID() == nodePosID))
//        return node;
//
//    // if target found in right, return node.
//    if ((node->getRight() != nullptr) &&
//        (node->getRight()->getNodePosID() == nodePosID))
//        return node;
//
//    // turn left
//    if (nodePosID < node->getNodePosID())
//        return (searchBranch(nodePosID, node->getLeft()));
//    else // or turn right
//        return (searchBranch(nodePosID, node->getRight()));
//
////    if (node == nullptr)
////        return nullptr;
////
////    if (node->getNodePosID() == nodePosID)
////        return node;
////    else if (node->getNodePosID() < nodePosID)
////        return this->searchBranch(nodePosID, node->getRight());
////    else
////        return this->searchBranch(nodePosID, node->getLeft());
//
//}


/**
 * Recursively find a node given a name in the tree.
 * @param name The node's name
 * @param node Current node. Start at anchor.
 * @return True if found the node match given name, otherwise false
 */
bool Tree::isNameFound(const std::string & name, TreeNode * node) const {

    if (node == nullptr)
        return false; //empty nodes are always false

    if (node->getName() == name) {
        std::cout << "Record ID: " << node->getNodeID();
        std::cout << ", Name: " << node->getName();
        std::cout << ", Age: " << node->getAlter();
        std::cout << ", Income: " << node->getEinkommen();
        std::cout << ", PLZ: " << node->getPLZ();
        std::cout << ", PosID: " << node->getNodePosID();
        std::cout << "\n";

        return true;
    } else {
        if (isNameFound(name, node->getLeft()))
            return true; //if node found in left side, don't need to search in the right side
        else
            return isNameFound(name, node->getRight());
    }
}


void Tree::printAll() const {
    std::cout << "Currently database holds " << this->nodeCounter << " records.\n";
    std::cout << "ID    | Name                 | Age |     Income |   PLZ |    Pos \n";
    std::cout << "======+======================+=====+============+=======+========\n";

    this->preOrderPrint(this->anker);
}


void Tree::preOrderPrint(TreeNode *node) const {
    if (node == nullptr) {
        return;
    }

    node->print();

    preOrderPrint(node->getLeft());

    preOrderPrint(node->getRight());

}


//
////////////////////////////////////